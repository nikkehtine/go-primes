package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	fmt.Println(isPrime(8))
}

func isPrime(number int) bool {
	fmt.Println(strconv.Itoa(number) + ":")

	if number == 1 || number == 0 {
		fmt.Println("0 and 1 are neither prime nor composite")
		return false
	}

	root := math.Sqrt(float64(number))
	if root == math.Trunc(root) {
		fmt.Println("The square root is", root)
		return false
	}

	i := 2
	intRoot := int(math.Trunc(root))
	for i <= intRoot {
		if number % i == 0 {
			fmt.Println("It's divisible by", i)
			return false
		}
		i++
	}

	// If it passed these checks, it most certainly is prime
	return true
}
