package main

import "testing"

type Primes struct {
	number int
	result bool
}

func TestPrime(t *testing.T) {

	testData := []Primes {
		{0,		false},
		{2137,	true},
		{2371,	true},
		{144,	false},
		{1, 	false},
		{2539,	true},
		{15077,	true},
		{8, 	false},
		{89, 	true},
		{58737, false},
	}

	for _, datum := range testData {
		result := isPrime(datum.number)

		if result != datum.result {
			t.Errorf("isPrime(%d) FAILED.\n Expected %t, got %t\n", datum.number, datum.result, result)
		} else {
			t.Logf("isPrime(%d) PASSED!\n Expected %t, got %t\n", datum.number, datum.result, result)
		}
	}
}